interface Rectangle {
    height: number,
    width: number
}
  
interface ColoredRectangle extends Rectangle{
    color: string
}
const rectangle: Rectangle = {
    height: 20,
    width: 10
}
console.log(rectangle);

const ColoredRectangle: ColoredRectangle = {
    width: 20,
    height: 20,
    color: "red"
}
console.log(ColoredRectangle)